Considering the technologies used for the project, all you need to get it up and running is a web-browser. Simply download files to your local machine and run index.html.

What is this repository for?
This repository is made to avoid building the same basics each time you need simple but robust adaptive multi-page form. I believe, this way we may truly progress instead of mastering typing speed. You're welcome to use it for any purposes, even commercial, completely free. You're even more welcome to improve the code to help others benefit also.

Version: 0.0.0 Blank and virgin

How do I get set up?
Everything you need to use the project as is or to start building your own solution upon it is already included. Just download the code and use your web-browser.

Who do I talk to?
To ask any questions or to propose improvements, please contact the repo owner.